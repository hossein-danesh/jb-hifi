﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JBHIFI.Common
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}
