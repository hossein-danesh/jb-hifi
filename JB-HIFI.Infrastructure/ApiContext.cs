﻿using JBHIFI.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using JBHIFI.Models;

namespace JBHIFI.Infrastructure
{
    public class ApiContext : IdentityDbContext<ApplicationUser>
    {
        public ApiContext(DbContextOptions<ApiContext> options): base(options)
        {
         
        }
        public DbSet<Product> Products { get; set; }
      
    }
}
