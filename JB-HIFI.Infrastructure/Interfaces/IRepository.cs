using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;

namespace JBHIFI.Infrastructure
{
    public interface IRepository<T> : IDisposable where T : class
    {
        IQueryable<T> AsQueryAble();
        IEnumerable<T> GetList(Expression<Func<T, bool>> predicate);
        T Get(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Delete(object id);
        void Update(T entity);
       
    }
}