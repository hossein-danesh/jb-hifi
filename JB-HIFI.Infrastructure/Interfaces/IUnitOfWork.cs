using System;
using JBHIFI.Common;
using Microsoft.EntityFrameworkCore;

namespace JBHIFI.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity;
        int SaveChanges();
    }
       
}