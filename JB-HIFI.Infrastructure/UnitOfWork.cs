using System;
using System.Collections.Generic;
using JBHIFI.Infrastructure;
using JBHIFI.Common;
using Microsoft.EntityFrameworkCore;

namespace JBHIFI.Infrastructure
{
    public class UnitOfWork :  IUnitOfWork
    {
        public UnitOfWork(ApiContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity
        {
            return (IRepository<TEntity>) new Repository<TEntity>(Context);
        }
        public ApiContext Context { get; }
        public int SaveChanges()
        {
            return Context.SaveChanges();
        }
        public void Dispose()
        {
            Context?.Dispose();
        }
    }
}