﻿using JBHIFI.Service.Model;
using System.Collections.Generic;

namespace JBHIFI.Service
{
    public interface IProductService
    {
        ProductDto Get(string id);
        List<ProductDto> Search(string name, string description, string model, string brand);
        string Create(ProductDto product);
        string Update(ProductDto product);
        bool Delete(string id);
    }
}
