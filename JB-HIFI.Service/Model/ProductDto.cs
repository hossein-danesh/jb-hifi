﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JBHIFI.Service.Model
{
    public class ProductDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
    }
}
