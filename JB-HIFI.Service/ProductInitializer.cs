﻿using AutoMapper;
using JBHIFI.Domain;
using JBHIFI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JBHIFI.Service
{
    public class ProductInitializer
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Product> _productRepository;
        private readonly IMapper _mapper;
        public ProductInitializer(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _productRepository = _unitOfWork.GetRepository<Product>();
            _mapper = mapper;
        }

        public async Task Seed()
        {
            _productRepository.Add(new Product() { Id = "1", Name = "Name1", Brand = "Brand1", Description = "Description1", Model = "Model1" });
            _productRepository.Add(new Product() { Id = "2", Name = "Name2", Brand = "Brand2", Description = "Description2", Model = "Model2" });
            _productRepository.Add(new Product() { Id = "3", Name = "Name3", Brand = "Brand3", Description = "Description3", Model = "Model3" });
            _productRepository.Add(new Product() { Id = "4", Name = "Name4", Brand = "Brand4", Description = "Description4", Model = "Model4" });
            _productRepository.Add(new Product() { Id = "5", Name = "Name5", Brand = "Brand5", Description = "Description5", Model = "Model5" });
            _productRepository.Add(new Product() { Id = "6", Name = "Name6", Brand = "Brand6", Description = "Description6", Model = "Model6" });
            _productRepository.Add(new Product() { Id = "7", Name = "Name7", Brand = "Brand7", Description = "Description7", Model = "Model7" });
            _productRepository.Add(new Product() { Id = "8", Name = "Name8", Brand = "Brand8", Description = "Description8", Model = "Model8" });
            _unitOfWork.SaveChanges();
        }
    }
}
