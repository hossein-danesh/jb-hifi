﻿using AutoMapper;
using JBHIFI.Domain;
using JBHIFI.Service.Model;
using JBHIFI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JBHIFI.Service
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Product> _productRepository;
        private readonly IMapper _mapper;
        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _productRepository = _unitOfWork.GetRepository<Product>();
            _mapper = mapper;
        }

        public string Create(ProductDto product)
        {
            var newProduct = _mapper.Map<Product>(product) ;
            _productRepository.Add(newProduct);
            _unitOfWork.SaveChanges();
            return newProduct.Id;
        }

        public bool Delete(string id)
        {
            _productRepository.Delete(id);
            _unitOfWork.SaveChanges();
            return true;
        }

        public ProductDto Get(string id)
        {
            var product = _productRepository.Get(p => p.Id == id);
            return _mapper.Map<ProductDto>(product);
            
        }
        
        public List<ProductDto> Search(string name, string description, string model, string brand)
        {
            IQueryable<Product> query = _productRepository.AsQueryAble();
            if (!string.IsNullOrEmpty(name))
                query = query.Where(p => p.Name.Contains(name));
            if (!string.IsNullOrEmpty(description))
                query = query.Where(p => p.Description.Contains(description));
            if (!string.IsNullOrEmpty(model))
                query = query.Where(p => p.Model.Contains(model));
            if (!string.IsNullOrEmpty(brand))
                query = query.Where(p => p.Brand.Contains(brand));

            var products = query.ToList();
            return _mapper.Map<List<ProductDto>>(products);
        }

        public string Update(ProductDto product)
        {
            string id = product.Id;
            var oldProduct = _productRepository.Get(p => p.Id == id);
            if (oldProduct == null)
                new KeyNotFoundException();
            oldProduct.Name = product.Name;
            _productRepository.Update(oldProduct);
            _unitOfWork.SaveChanges();
            return oldProduct.Id;
        }


    }
}
