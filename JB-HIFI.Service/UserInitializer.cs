﻿using JBHIFI.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace JBHIFI.Service
{
    public class UserInitializer
    {
        private UserManager<ApplicationUser> _userManager;


        public UserInitializer(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
  
        }
        public async Task Seed()
        {
          var  result=  await _userManager.CreateAsync(new ApplicationUser() {  UserName = "JbUser", Email = "Jb@Email.com" }, "123456");

        }
    }
}
