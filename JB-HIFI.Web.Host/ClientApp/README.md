##Project specification
This part is implemented by Angular CLI version 7.0.3 .

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `npm install`on ClientApp folder to download and install angular packages.
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Test the application
Run `npm install`on ClientApp folder to download and install angular packages.
by runnig `ng serve` client side is ready for testing.
