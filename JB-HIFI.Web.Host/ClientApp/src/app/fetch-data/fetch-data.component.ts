import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent implements OnInit {
  public products: Product[];
  public name: string;
  public brand: string;
  public model: string;
  public description: string;
  public http: HttpClient
  public params = new HttpParams();
  readonly baseUrl = 'http://localhost:61854/';
  constructor(httpClient: HttpClient) {
    this.http = httpClient;
    this.name = "";
    this.brand = "";
    this.model = "";
    this.description = "";
    this.getProducts();
  }
  onClickSearch() {
    this.getProducts();
  }
  ngOnInit() {
    
  };
  getProducts() {

    
    this.http.get<Product[]>(this.baseUrl + 'Products?Name=' + this.name + '&brand=' + this.brand + '&model=' + this.model + '&description=' + this.description, {
      responseType: "json",
      params: this.params
    }).subscribe(result => {
      this.products = result;
      console.log(this.products);
    }, error => console.error(error));}
}

interface Product {
  id: string;
  name: string;
  description: string;
  brand: string;
}
