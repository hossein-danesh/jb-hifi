﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JBHIFI.Service;
using JBHIFI.Service.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JBHIFI.Web.Controllers
{
    [Route("products")]
    [ApiController]
    [Authorize]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        [HttpGet]
        public ActionResult<IEnumerable<ProductDto>> Search([FromQuery] string Name,[FromQuery] string Model, [FromQuery]  string Brand, [FromQuery] string Description)
        {
            var result= _productService.Search(Name,Description, Model, Brand);
            return Ok(result??new List<ProductDto>());
        }

        [HttpGet("{id}", Name = "Get")]
        [ProducesResponseType(typeof(ProductDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<ProductDto> Get(string id)
        {
           var product= _productService.Get(id);
            if (product == null)
               return NotFound();
            return Ok(product);
        }

            [HttpPost("{id}")]
        public string Post(ProductDto product )
        {
            return _productService.Create(product);
        }

        [HttpPut("{id}")]
        public string Put(string id, [FromBody] ProductDto product)
        {
            product.Id = id;
            return _productService.Update(product);
        }

        [HttpDelete("{id}")]
        public bool Delete(string id)
        {
            return _productService.Delete(id);
        }
    }
}
