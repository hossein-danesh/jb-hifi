﻿using AutoMapper;
using JBHIFI.Domain;
using JBHIFI.Service.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace JBHIFI.Infrastructure
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<ProductDto, Product>();
            CreateMap< Product, ProductDto>();
        }
    }
}
