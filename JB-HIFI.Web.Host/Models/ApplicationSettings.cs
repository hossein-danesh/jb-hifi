﻿namespace JBHIFI.Web.Controllers
{
    public partial class ApplicationUserController
    {
        public class ApplicationSettings
        {
            public string JWT_Secret { get; set; } = "HoSsEiN123456!@#$123";
            public string Client_URL { get; set; } = "http://localhost:61854";
        }
    }
}