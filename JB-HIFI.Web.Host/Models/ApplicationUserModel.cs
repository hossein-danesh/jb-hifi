﻿namespace JBHIFI.Web.Controllers
{
    public partial class ApplicationUserController
    {
        public class ApplicationUserModel
        {
            public string UserName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public string FullName { get; set; }
        }
    }
}