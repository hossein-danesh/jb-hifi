﻿namespace JBHIFI.Web.Controllers
{
    public partial class ApplicationUserController
    {
        public class LoginModel
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }
    }
}