##Project specification
This is a 3 layered application using Angular CLI version 7.0.3 on top of the web api services.
Dot net core 2.1, entity framework core are the used technologies

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
By running `npm install` on ClientApp directory, all packages for building project will be downloaded and installed.
Then,Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Test the application

Run `ng serve` to build and serve client side and also by runnig `dotnet run` back-end side is ready to test.
All pages are protected and can be accessed by blow credential:
UserName:JbUser
Password:123456

JWT is selected to authenticate and authorize the user, send information that can be verified and trusted by means of a digital signature. It comprises a compact and URL-safe JSON object, which is cryptographically signed to verify its authenticity

## Running unit tests

dotnet test - .NET test driver used to execute unit tests.
