﻿using JBHIFI.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace JBHIFI.Infrastructure
{
    public class ApiTestContext : IdentityDbContext
    {
        public ApiTestContext(DbContextOptions<ApiTestContext> options)
            : base(options)
        {
         
        }
        public DbSet<Product> Products { get; set; }
    }
}
