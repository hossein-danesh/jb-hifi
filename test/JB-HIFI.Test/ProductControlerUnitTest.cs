﻿using AutoMapper;
using JBHIFI.Domain;
using JBHIFI.Infrastructure;
using JBHIFI.Service;
using JBHIFI.Service.Model;
using JBHIFI.Web.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace JBHIFI.Test
{
    public class ProductControlerUnitTest
    {
        private IProductService _productService;
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        private List<Product> _products;
        private IRepository<Product> _productRepository;
        private HttpContext _contextMock;
        private HttpResponseMessage _response;
        private ApiTestContext _productsContext;
        private const string ServiceBaseURL = "http://localhost:61854/";
        [Test]
        public void SearchTest()
        {
            //arrange
            var expected = new ProductDto() { Id = "1" };
            var productController = new ProductController(_productService);
            productController.ControllerContext.HttpContext = _contextMock;
            //act
            var result = productController.Search("Model1","Model1","Brand1",null);
            //assert

            var viewResult = result.Result as OkObjectResult;
            Assert.AreEqual(viewResult.StatusCode, StatusCodes.Status200OK);


        }
        [Test]
        public void GetProductByIdTest()
        {
            //arrange
            var expected = new ProductDto() { Id = "2", Name = "Name2", Brand = "Brand2", Description = "Description2", Model = "Model2" };
            var productController = new ProductController(_productService);
            productController.ControllerContext.HttpContext = _contextMock;
            //act
            var result = productController.Get("2");
            //assert
            var viewResult = result.Result as OkObjectResult;
            Assert.AreEqual(viewResult.StatusCode, StatusCodes.Status200OK);

        }
        [Test]
        public void CreateProductTest()
        {
            //arrange
            var product = new ProductDto() { Id = $"{_products.Count + 1}", Name = "Name9", Brand = "Brand9", Description = "Description9", Model = "Model9" };
            var productController = new ProductController(_productService);
            productController.ControllerContext.HttpContext = _contextMock;
            //act
            var _response = productController.Post(product);

            //assert
            Assert.AreEqual(_response, product.Id);

        }
        [Test]
        public void UpdateProductTest()
        {
            //arrange
            var product = new ProductDto() { Id = "2", Name = "Name9", Brand = "Brand9", Description = "Description9", Model = "Model9" };
            var productController = new ProductController(_productService);
            productController.ControllerContext.HttpContext = _contextMock;
            //act
            var _response = productController.Put(product.Id, product);
            //assert
            Assert.AreEqual(_response, product.Id);
        }
        [Test]
        public void DeleteProductTest()
        {
            //arrange
            var productController = new ProductController(_productService);
            productController.ControllerContext.HttpContext = _contextMock;
            var productCount = _products?.Count ?? 1;
            //act
            var _response = productController.Delete("4");
            //assert
            Assert.AreEqual(_products.Count, productCount - 1);
        }
        [OneTimeSetUp]
        public void Setup()
        {
            var expectedProductDto = new ProductDto()
            {
                Id = "2",
                Brand = "2",
                Description = "Description2",
                Model = "Model2",
                Name = "Name2"
            };
            var expectedProduct = new Product()
            {
                Id = "2",
                Brand = "2",
                Description = "Description2",
                Model = "Model2",
                Name = "Name2"
            };
            _products = SetUpProducts();
            _productRepository = SetUpProductRepository();
            var unitOfWork = new Mock<IUnitOfWork>();
            var mockMapper = new Mock<IMapper>();
            unitOfWork.Setup(x => x.GetRepository<Product>()).Returns(_productRepository);
            mockMapper.Setup(x => x.Map<Product>(It.IsAny<ProductDto>()))
                .Returns(expectedProduct);
            mockMapper.Setup(x => x.Map<ProductDto>(It.IsAny<Product>()))
               .Returns(expectedProductDto);

            _unitOfWork = unitOfWork.Object;
            _mapper = mockMapper.Object;
            _productService = new ProductService(_unitOfWork, _mapper);

            var userMock = new Mock<IPrincipal>();
            userMock.Setup(p => p.IsInRole("admin")).Returns(true);

            var contextMock = new Mock<HttpContext>();
            contextMock.Setup(x => x.User).Returns(new ClaimsPrincipal());
            _contextMock = contextMock.Object;
        }
        private static List<Product> SetUpProducts()
        {
            var products = ProductDataInitializer.GetAllProducts();
            return products;
        }
        private IRepository<Product> SetUpProductRepository()
        {

            var mockRepo = new Mock<IRepository<Product>>();

            mockRepo.Setup(p => p.GetList(It.IsAny<Expression<Func<Product, bool>>>())).Returns(_products);
            mockRepo.Setup(p => p.AsQueryAble()).Returns(_products.AsQueryable());
            mockRepo.Setup(p => p.Get(It.IsAny<Expression<Func<Product, bool>>>()))
                .Returns(_products.FirstOrDefault(p => p.Id == "2"));

            mockRepo.Setup(p => p.Add((It.IsAny<Product>())))
                .Callback(new Action<Product>(newProduct =>
                {
                    newProduct.Id = $"{_products.Count + 1}";

                    _products.Add(newProduct);
                }));

            mockRepo.Setup(p => p.Update(It.IsAny<Product>()))
                .Callback(new Action<Product>(prod =>
                {
                    var oldProduct = _products.Find(a => a.Id == prod.Id);
                    oldProduct.Name = prod.Name;
                    oldProduct.Brand = prod.Brand;
                    oldProduct.Model = prod.Model;
                    oldProduct.Description = prod.Description;
                }));

            mockRepo.Setup(p => p.Delete(It.IsAny<object>()))
                .Callback(new Action<object>(prod =>
                {
                    var productToRemove =
                        _products.Find(a => a.Id == prod);

                    if (productToRemove != null)
                        _products.Remove(productToRemove);
                }));

            return mockRepo.Object;
        }
    }
}

