﻿using JBHIFI.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace JBHIFI.Test
{
    public class ProductDataInitializer
    {
        public static List<Product> GetAllProducts()
        {
            var products = new List<Product>()
            {
                new Product(){ Id="1",Name="Name1",Brand="Brand1" ,Description="Description1",Model="Model1" },
                new Product(){ Id="2",Name="Name2",Brand="Brand2" ,Description="Description2",Model="Model2" },
                new Product(){ Id="3",Name="Name3",Brand="Brand3" ,Description="Description3",Model="Model3" },
                new Product(){ Id="4",Name="Name4",Brand="Brand4" ,Description="Description4",Model="Model4" },
                new Product(){ Id="5",Name="Name5",Brand="Brand5" ,Description="Description5",Model="Model5" },
                new Product(){ Id="6",Name="Name6",Brand="Brand6" ,Description="Description6",Model="Model6" },
                new Product(){ Id="7",Name="Name7",Brand="Brand7" ,Description="Description7",Model="Model7" },
                new Product(){ Id="8",Name="Name8",Brand="Brand8" ,Description="Description8",Model="Model8" }

            };

            return products;
        }
    }

}
